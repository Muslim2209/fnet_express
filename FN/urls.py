from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='FN Express API')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('areas/', include('areas.urls')),
    path('delivery/', include('delivery.urls')),
    path('tracking/', include('tracking.urls')),
    path('users/', include('users.urls')),
    url(r'^$', schema_view),
    url(r'^api-auth/', include('rest_framework.urls'))
]
