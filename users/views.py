from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser, IsAuthenticated, AllowAny

from permissions.permissions import IsAdminOrIsSameUser
from users.models import ExpressUser
from users.serializers import ExpressUserUserSerializer


class ExpressUserViewSet(viewsets.ModelViewSet):
    queryset = ExpressUser.objects.all()
    serializer_class = ExpressUserUserSerializer

    def get_permissions(self):
        if self.action == 'create':
            permission_classes = [AllowAny]
        elif self.action in ['update', 'partial_update']:
            permission_classes = [IsAuthenticated, IsAdminOrIsSameUser]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]
