from django.contrib import admin
from django.contrib.auth.admin import UserAdmin, AdminPasswordChangeForm
from django.contrib.auth.models import Group

from users.forms import UserCreationForm, UserChangeForm
from users.models import ExpressUser


class ExpressUserAdmin(UserAdmin):
    list_display = ['phone_number', 'first_name', 'user_type', 'is_active']
    ordering = ['-date_joined']
    form = UserChangeForm
    add_form = UserCreationForm
    change_password_form = AdminPasswordChangeForm
    fieldsets = (
        (None, {'fields': ('phone_number', 'first_name', 'last_name', 'car_info', 'passport_sn', 'image', 'password')}),
        ('Permissions', {'fields': ('user_type', 'is_staff',
                                    'is_active', 'is_superuser')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'phone_number', 'first_name', 'last_name', 'car_info', 'passport_sn', 'image', 'user_type',
                'is_active', 'is_staff', 'password1', 'password2',),
        }),
    )
    search_fields = ('phone_number', 'first_name', 'last_name', 'user_type')


admin.site.register(ExpressUser, ExpressUserAdmin)

admin.site.unregister(Group)
