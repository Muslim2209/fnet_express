from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField

from users.managers import ExpressUserManager


class ExpressUser(AbstractBaseUser, PermissionsMixin):
    DRIVER = 'DRIVER'
    COURIER = 'COURIER'
    OFFICE_MANAGER = 'MANAGER'
    USER_TYPES = (
        (DRIVER, 'Driver'),
        (COURIER, 'Courier'),
        (OFFICE_MANAGER, 'Ofis menejer'),
    )
    phone_number = PhoneNumberField(unique=True)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150, blank=True)
    car_info = models.CharField(max_length=200, blank=True)
    passport_sn = models.CharField(max_length=10, null=True, blank=True)
    image = models.ImageField(null=True, blank=True)
    user_type = models.CharField(max_length=7, choices=USER_TYPES)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=timezone.now)

    objects = ExpressUserManager()

    USERNAME_FIELD = 'phone_number'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    def __str__(self):
        return '{} {}'.format(self.user_type, self.first_name, self.phone_number)
