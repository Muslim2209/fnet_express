from rest_framework import serializers

from users.models import ExpressUser


class ExpressUserUserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(style={"input_type": "password"}, write_only=True)

    class Meta:
        model = ExpressUser
        fields = ['phone_number', "first_name", "last_name", "passport_sn", "user_type", "car_info", "password"]

    def create(self, validated_data):
        password = validated_data.pop('password')
        # groups = validated_data.pop('groups')
        # user_permissions = validated_data.pop('user_permissions')
        user = ExpressUser.objects.create(**validated_data)
        user.set_password(password)
        # for group in groups:
        #     user.add(group)
        # for permission in user_permissions:
        #     user.add(permission)
        user.save()
        return user
