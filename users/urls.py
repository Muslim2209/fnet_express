from django.urls import path, include
from rest_framework import routers

from users.views import ExpressUserViewSet

router = routers.DefaultRouter()
router.register('', ExpressUserViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
