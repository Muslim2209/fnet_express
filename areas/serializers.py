from rest_framework import serializers

from areas.models import Region, Country, City  # , Address


class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = '__all__'


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = '__all__'


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'

# class AddressSerializer(serializers.ModelSerializer):
#     city = CitySerializer()
#
#     class Meta:
#         model = Address
#         fields = '__all__'
#
#     def create(self, validated_data):
#         city_data = validated_data.pop('city')
#         new_city, _ = City.objects.get_or_create(**city_data)
#         validated_data['city'] = new_city
#         address_data = Address.objects.create(**validated_data)
#         return address_data
