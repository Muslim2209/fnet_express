from django.contrib import admin

from areas.models import Region, Country, City  # , Address

admin.site.register(Country)
admin.site.register(Region)
admin.site.register(City)
# admin.site.register(Address)
