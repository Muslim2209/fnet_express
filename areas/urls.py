from django.urls import path, include
from rest_framework import routers

from areas.views import CityViewSet, CountryViewSet, RegionViewSet  # ,AddressViewSet

router = routers.DefaultRouter()
# router.register('address', AddressViewSet)
router.register('country', CountryViewSet)
router.register('region', RegionViewSet)
router.register('city', CityViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
