from django.db import models


class Country(models.Model):
    name = models.CharField(max_length=200, unique=True)

    class Meta:
        verbose_name_plural = 'Countries'

    def __str__(self):
        return self.name


class Region(models.Model):
    name = models.CharField(max_length=160, unique=True)
    country = models.ForeignKey('Country', on_delete=models.PROTECT)

    def __str__(self):
        return self.name


class City(models.Model):
    name = models.CharField(max_length=160)
    region = models.ForeignKey('Region', on_delete=models.PROTECT)

    class Meta:
        verbose_name_plural = 'Cities'

    def __str__(self):
        return '{} ({})'.format(self.name, self.region)


# class Address(models.Model):
#     city = models.ForeignKey('City', on_delete=models.PROTECT)
#     street = models.CharField(max_length=200)
#     house = models.CharField(max_length=120)
#     reference_point = models.CharField(max_length=400, blank=True)
#
#     class Meta:
#         verbose_name_plural = 'Addresses'
#
#     def __str__(self):
#         return '{} {}'.format(self.city, self.reference_point[:20])
