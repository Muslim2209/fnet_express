from django.urls import path, include
from rest_framework import routers

from tracking.views import TrackViewSet

router = routers.DefaultRouter()
router.register('', TrackViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
