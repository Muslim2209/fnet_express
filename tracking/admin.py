from django.contrib import admin
from django.contrib.gis.admin import OSMGeoAdmin

from tracking.models import Track


class TrackAdmin(OSMGeoAdmin):
    list_display = ['order', 'where_is', 'location', 'time']


admin.site.register(Track, TrackAdmin)
