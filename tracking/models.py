from django.contrib.gis.db import models as geo_models
from django.db import models
from django.utils import timezone


class Track(models.Model):
    order = models.ForeignKey('delivery.Order', on_delete=models.CASCADE)
    where_is = models.CharField(max_length=120, blank=True)
    location = models.DecimalField(max_digits=9, decimal_places=6)
    # location = geo_models.PointField(blank=True)
    time = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return '(Location) {} -- (TO) {}'.format(self.where_is, self.order.location_to)
