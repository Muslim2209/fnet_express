from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, IsAdminUser

from delivery.models import Order
from permissions.permissions import IsAdminOrDriverOwner
from tracking.models import Track
from tracking.serializers import TrackSerializer


class TrackViewSet(viewsets.ModelViewSet):
    queryset = Track.objects.all()
    serializer_class = TrackSerializer

    def get_queryset(self):
        queryset = Track.objects.all()
        user = self.request.user
        try:
            if user.user_type == 'DRIVER':
                order = Order.objects.get(driver=user)
                queryset = queryset.filter(order=order)
            if user.user_type == 'COURIER':
                order = Order.objects.get(courier=user)
                queryset = queryset.filter(order=order)
        except Exception:
            pass
        return queryset

    def get_permissions(self):
        if self.action == 'list':
            permission_classes = [IsAuthenticated]
        elif self.action == 'create':
            permission_classes = [IsAuthenticated, IsAdminOrDriverOwner]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]
