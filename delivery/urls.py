from django.urls import path, include
from rest_framework import routers

from delivery.views import (OrderViewSet, ParcelViewSet,
                            PaymentViewSet, CallView, TariffViewSet)

router = routers.DefaultRouter()
router.register('order', OrderViewSet)
router.register('parcel', ParcelViewSet)
router.register('payment', PaymentViewSet)
router.register('tariff', TariffViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('call/', CallView.as_view()),
]
