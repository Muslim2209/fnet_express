from rest_framework import viewsets, generics
from rest_framework.permissions import IsAuthenticated, IsAdminUser, AllowAny

from delivery.models import Order, Payment, Parcel, Call, Tariff
from delivery.serializers import (OrderSerializer, ParcelSerializer, OrderUpdateSerializer,
                                  PaymentSerializer, CallSerializer, TariffSerializer)
from permissions.permissions import IsAdminOrCourierOwner, IsAdminOrDriverOwner


class TariffViewSet(viewsets.ModelViewSet):
    queryset = Tariff.objects.all()
    serializer_class = TariffSerializer


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()

    def get_serializer_class(self):
        serializer_class = OrderSerializer
        if self.action in ['put', 'partial_update']:
            serializer_class = OrderUpdateSerializer
        return serializer_class

    def get_permissions(self):
        if self.action in ['list', 'retrieve']:
            permission_classes = [IsAuthenticated, (IsAdminUser | IsAdminOrCourierOwner | IsAdminOrDriverOwner)]
        elif self.action in ['update', 'partial_update', 'destroy']:
            permission_classes = [IsAuthenticated, (IsAdminUser | IsAdminOrCourierOwner)]
        else:
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]

    def perform_update(self, serializer):
        total_weight = 0
        total_volume = 0
        total_price = 0
        for parcel in serializer.validated_data['parcel']:
            total_weight += parcel['weight']
            total_volume += parcel['volume']
            total_price += parcel['price']

        serializer.validated_data['total_weight'] = total_weight
        serializer.validated_data['total_volume'] = total_volume
        serializer.validated_data['total_price'] = total_price
        serializer.save()


class PaymentViewSet(viewsets.ModelViewSet):
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer

    # def perform_create(self, serializer):
    #     order = Order.objects.get(id=serializer.validated_data['order'].id)
    #     order.remain_price -= serializer.validated_data['amount']
    #     order.save()
    #     serializer.save(order=order)

    # def perform_destroy(self, instance):
    #     order = Order.objects.get(id=instance.order.id)
    #     order.remain_price += instance.amount
    #     order.save()
    #     instance.delete()

    def get_queryset(self):
        queryset = Payment.objects.all()
        user = self.request.user
        if user.user_type == 'DRIVER':
            order = Order.objects.get(driver=user)
            queryset = queryset.filter(order=order)
        if user.user_type == 'COURIER':
            order = Order.objects.get(courier=user)
            queryset = queryset.filter(order=order)
        return queryset

    def get_permissions(self):
        if self.action in ['list', 'create']:
            permission_classes = [IsAuthenticated]
        elif self.action in ['retrieve', 'update', 'partial_update', 'destroy']:
            permission_classes = [IsAuthenticated, (IsAdminUser | IsAdminOrCourierOwner)]
        else:
            permission_classes = []
        return [permission() for permission in permission_classes]


class ParcelViewSet(viewsets.ModelViewSet):
    queryset = Parcel.objects.all()
    serializer_class = ParcelSerializer

    def get_queryset(self):
        queryset = Parcel.objects.all()
        user = self.request.user
        if user.user_type == 'DRIVER':
            order = Order.objects.get(driver=user)
            queryset = queryset.filter(order=order)
        if user.user_type == 'COURIER':
            order = Order.objects.get(courier=user)
            queryset = queryset.filter(order=order)
        return queryset

    def get_permissions(self):
        if self.action == 'retrieve':
            permission_classes = [IsAdminOrDriverOwner | IsAdminOrCourierOwner]
        elif self.action in ['update', 'partial_update', 'destroy']:
            permission_classes = [IsAuthenticated, IsAdminOrCourierOwner]
        else:
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]


class CallView(generics.CreateAPIView):
    queryset = Call.objects.all()
    serializer_class = CallSerializer
    permission_classes = [AllowAny]
