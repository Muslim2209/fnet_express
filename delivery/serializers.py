from rest_framework import serializers

from delivery.models import Order, Parcel, Payment, Call, Tariff


class TariffSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tariff
        fields = '__all__'


class ParcelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Parcel
        fields = '__all__'


class ParcelFromOrderSerializer(serializers.ModelSerializer):
    order = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Parcel
        fields = '__all__'

    def validate(self, attrs):
        if attrs['weight'] is None:
            attrs['weight'] = 1
        if attrs['volume'] is None:
            attrs['volume'] = 0

        return attrs


class ParcelUpdateFromOrderSerializer(serializers.ModelSerializer):
    order = serializers.PrimaryKeyRelatedField(read_only=True)
    id = serializers.IntegerField(required=True)

    class Meta:
        model = Parcel
        fields = '__all__'

    def validate(self, attrs):
        if attrs['weight'] is None:
            attrs['weight'] = 1
        if attrs['volume'] is None:
            attrs['volume'] = 0
        if 'id' not in attrs:
            raise serializers.ValidationError({"error": "id required field"})
        return attrs


class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = '__all__'

    def validate(self, attrs):
        if None in attrs.values() or '' in attrs.values():
            raise serializers.ValidationError('Please fill all inputs')
        if attrs['amount'] < 500:
            raise serializers.ValidationError('Too small amount')
        return attrs


class PaymentFromOrderSerializer(serializers.ModelSerializer):
    order = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Payment
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):
    parcel = ParcelFromOrderSerializer(many=True)
    payment = PaymentFromOrderSerializer(required=False)
    courier = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Order
        fields = '__all__'

    def create(self, validated_data):
        parcel_data = validated_data.pop('parcel')
        payment_data = validated_data.pop('payment')
        order = Order.objects.create(**validated_data)
        total_price = 0
        total_weight = 0
        total_volume = 0
        tariff = validated_data['tariff']
        for parcel in parcel_data:
            if parcel['parcel_type'] == 'CRG':
                parcel['price'] = tariff.price_1
                if parcel['weight'] > 1:
                    more = parcel['weight'] // 1
                    if float(parcel['weight']).is_integer():
                        more -= 1
                    parcel['price'] += more * tariff.price_more_1
            if parcel['parcel_type'] == 'FRA':
                parcel['price'] = tariff.price_1_tech
                if parcel['weight'] > 1:
                    more = parcel['weight'] // 1
                    if float(parcel['weight']).is_integer():
                        more -= 1
                    parcel['price'] += more * tariff.price_more_1_tech
            if parcel['parcel_type'] == 'MON':
                if parcel['weight'] <= 1000000:
                    parcel['price'] = tariff.price_1_million
                elif 1000000 < parcel['weight'] <= 5000000:
                    parcel['price'] = tariff.price_1_5_million
                elif 5000000 < parcel['weight'] <= 25000000:
                    parcel['price'] = tariff.price_5_25_million
                else:
                    parcel['price'] = tariff.price_more_25_million
            total_price += parcel['price']
            total_weight += parcel['weight']
            total_volume += parcel['volume']
            Parcel.objects.create(order=order, **parcel)
        updated_order = Order.objects.get(id=order.id)
        updated_order.total_price = updated_order.remain_price = total_price
        updated_order.total_weight = total_weight
        updated_order.total_volume = total_volume
        updated_order.save()
        if payment_data is not None:
            Payment.objects.create(order=updated_order, **payment_data)
        return updated_order

    def to_representation(self, instance):
        self.fields['payment'] = PaymentFromOrderSerializer(instance.payment.all(), many=True)
        return super(OrderSerializer, self).to_representation(instance)


class OrderUpdateSerializer(serializers.ModelSerializer):
    parcel = ParcelUpdateFromOrderSerializer(many=True)
    payment = PaymentFromOrderSerializer()
    courier = serializers.HiddenField(default=serializers.CurrentUserDefault())
    id = serializers.IntegerField()

    class Meta:
        model = Order
        fields = '__all__'

    def update(self, instance, validated_data):
        all_parcel_data = validated_data.pop('parcel')
        payment_data = validated_data.pop('payment')
        for parcel in all_parcel_data:
            if 'id' in parcel:
                parcel_id = parcel.pop('id')
                Parcel.objects.update_or_create(id=parcel_id, defaults=parcel)
            else:
                order_id = validated_data['id']
                parcel = Parcel.objects.create(order_id=order_id, **parcel)
                instance.parcel.add(parcel)
        instance_payment = instance.payment.all().values('amount').first()
        validated_data['remain_price'] = validated_data['remain_price'] + instance_payment['amount'] - payment_data[
            'amount']
        payment, created_payment = Payment.objects.update_or_create(order_id=instance.id, defaults=payment_data)
        instance.payment.add(payment)

        return super(OrderUpdateSerializer, self).update(instance, validated_data)

    def to_representation(self, instance):
        self.fields['payment'] = PaymentFromOrderSerializer(instance.payment.all(), many=True)
        return super(OrderUpdateSerializer, self).to_representation(instance)


class CallSerializer(serializers.ModelSerializer):
    class Meta:
        model = Call
        fields = '__all__'
