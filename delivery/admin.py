from django.contrib import admin

from delivery.models import Parcel, Order, Payment, Call, Tariff, FromTo  # ,UserBalance, Transaction

admin.site.register(Parcel)
admin.site.register(FromTo)


class TariffAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('name',)
        }),
        ('Hujjat, kiyim, buyum', {
            'fields': ('price_1', 'price_more_1', 'price_1_outside', 'price_2_5', 'price_more_5')
        }),
        ('Texnika, elektron jihoz', {
            'fields': ('price_1_tech', 'price_more_1_tech')
        }),
        ('Naqd pul', {
            'fields': ('price_1_million', 'price_1_5_million', 'price_5_25_million', 'price_more_25_million')
        })
    )


admin.site.register(Tariff, TariffAdmin)


# admin.site.register(UserBalance)
# admin.site.register(Transaction)


class CallAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Call._meta.get_fields() if field.name != 'id']
    list_editable = ['status']
    search_fields = ['status']


admin.site.register(Call, CallAdmin)


class OrderAdmin(admin.ModelAdmin):
    list_display = ['track_id', 'status', 'driver', 'courier', 'remain_price', 'reference_point']
    list_editable = ['status']


admin.site.register(Order, OrderAdmin)


class PaymentAdmin(admin.ModelAdmin):
    def delete_queryset(self, request, queryset):
        for obj in queryset:
            obj.delete()


admin.site.register(Payment, PaymentAdmin)
