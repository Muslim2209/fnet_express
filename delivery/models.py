from django.conf import settings
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class Tariff(models.Model):
    name = models.CharField(max_length=64)
    price_1 = models.PositiveIntegerField(verbose_name='1kg gacha', blank=True, null=True)
    price_1_tech = models.PositiveIntegerField(verbose_name='1kg gacha texnika', blank=True, null=True)
    price_more_1 = models.PositiveIntegerField(verbose_name='1kg dan oshgan har bir kg', blank=True, null=True)
    price_more_1_tech = models.PositiveIntegerField(verbose_name='1kg dan oshgan har bir kg texnika', blank=True,
                                                    null=True)
    price_1_outside = models.PositiveIntegerField(verbose_name='1kg gacha (Toshkent vil.)', blank=True, null=True)
    price_2_5 = models.PositiveIntegerField(verbose_name='2-5kg', blank=True, null=True)
    price_more_5 = models.PositiveIntegerField(verbose_name="5kg dan oshgan har bir kg", blank=True, null=True)
    price_1_million = models.PositiveIntegerField(verbose_name="1mln. gacha", blank=True, null=True)
    price_1_5_million = models.PositiveIntegerField(verbose_name="1-5mln", blank=True, null=True)
    price_5_25_million = models.PositiveIntegerField(verbose_name="5-25mln", blank=True, null=True)
    price_more_25_million = models.PositiveIntegerField(verbose_name="25mln. dan ko'p", blank=True, null=True)

    def __str__(self):
        return "{}".format(self.name)


class FromTo(models.Model):
    from_point = models.ForeignKey('areas.City', related_name='fromCity', on_delete=models.CASCADE)
    to_point = models.ForeignKey('areas.City', related_name='toCity', on_delete=models.CASCADE)

    def __str__(self):
        return "{} - {}".format(self.from_point, self.to_point)

    class Meta:
        ordering = ['from_point', 'to_point']
        unique_together = ['from_point', 'to_point']


class Parcel(models.Model):
    FRAGILE = 'FRA'
    MONEY = 'MON'
    CARGO = 'CRG'
    TYPES = (
        (FRAGILE, 'Sinuvchan buyum'),
        (MONEY, 'Pul'),
        (CARGO, 'Yuk')
    )
    number = models.PositiveIntegerField()
    parcel_type = models.CharField(max_length=12, choices=TYPES, default=FRAGILE)
    weight = models.DecimalField(verbose_name="og'irlik yoki qiymat(pul)", decimal_places=2, max_digits=4, blank=True,
                                 null=True)
    volume = models.DecimalField(decimal_places=2, max_digits=4, default=0)
    price = models.PositiveIntegerField(default=0)
    order = models.ForeignKey('Order', on_delete=models.CASCADE, related_name='parcel')

    def __str__(self):
        return 'Parcel №{}'.format(self.number)


class Order(models.Model):
    FROM_HOME_TO_HOME = 'HOHO'
    FROM_OFFICE_TO_HOME = 'OFHO'
    FROM_HOME_TO_OFFICE = 'HOOF'
    FROM_OFFICE_TO_OFFICE = 'OFOF'
    CATEGORIES = (
        (FROM_HOME_TO_HOME, 'Uydan uygacha'),
        (FROM_OFFICE_TO_HOME, 'Ofisdan uygacha'),
        (FROM_HOME_TO_OFFICE, 'Uydan ofisgacha'),
        (FROM_OFFICE_TO_OFFICE, 'Ofisdan ofisgacha'),
    )
    STATUS_CREATED = 'CRE'
    STATUS_ON_THE_WAY = 'ONW'
    STATUS_COMPLETED = 'CMP'
    STATUS_CANCELED = 'CNC'
    STATUSES = (
        (STATUS_CREATED, 'Created'),
        (STATUS_ON_THE_WAY, 'On the way'),
        (STATUS_COMPLETED, 'Completed'),
        (STATUS_CANCELED, 'Canceled'),
    )
    track_id = models.CharField(max_length=64, unique=True)
    status = models.CharField(max_length=3, choices=STATUSES, default=STATUS_CREATED)
    category = models.CharField(max_length=4, choices=CATEGORIES, default=FROM_OFFICE_TO_OFFICE)
    total_weight = models.DecimalField(decimal_places=2, max_digits=4, blank=True, null=True)
    total_volume = models.PositiveIntegerField(null=True, blank=True)
    total_price = models.PositiveIntegerField(default=0)
    remain_price = models.IntegerField(null=True, blank=True, default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    street = models.CharField(max_length=300, blank=True)
    house = models.PositiveSmallIntegerField(null=True, blank=True)
    reference_point = models.CharField(max_length=200, blank=True)
    location_from = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    location_to = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    driver = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, related_name='driver')
    courier = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, related_name='courier')
    from_to = models.ForeignKey('FromTo', on_delete=models.CASCADE)
    tariff = models.ForeignKey('Tariff', on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.track_id

    def pay(self, amount, payment_id=None):
        if amount is None:
            return None
        if payment_id is None:
            self.remain_price -= amount
            self.save()
        else:
            old_amount = Payment.objects.get(id=payment_id).amount
            difference = old_amount - amount
            self.remain_price += difference
            self.save()

    def unpay(self, payment_id):
        amount = Payment.objects.get(id=payment_id).amount
        self.remain_price += amount
        self.save()


class Payment(models.Model):
    CARD = 'CARD'
    CASH = 'CASH'
    TRANSFER = 'TRANSFER'
    METHODS = (
        (CARD, 'Online credit card payment'),
        (CASH, 'Payment at delivery'),
        (TRANSFER, 'Payment via transfer'),
    )
    method = models.CharField(max_length=8, choices=METHODS, default=CASH)
    amount = models.PositiveIntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    order = models.ForeignKey('Order', on_delete=models.PROTECT, related_name='payment')

    def __str__(self):
        return '{} so\'m to\'lov via {}'.format(self.amount, self.method)

    def save(self, *args, **kwargs):
        if self.amount == 0:
            return None
        if (self.id,) in Payment.objects.values_list('id'):
            self.order.pay(self.amount, self.id)
            super(Payment, self).save(*args, **kwargs)
        else:
            self.order.pay(self.amount)
            super(Payment, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        if self.amount != 0:
            self.order.unpay(self.id)
        return super(Payment, self).delete(*args, **kwargs)


# class Transaction(models.Model):
#     transaction_id = models.UUIDField()
#     order = models.ForeignKey('Order', on_delete=models.PROTECT)
#     amount = models.PositiveIntegerField()
#     balance = models.OneToOneField('UserBalance', on_delete=models.PROTECT)
#     created_at = models.DateTimeField(auto_now_add=True)
#     updated_at = models.DateTimeField(auto_now=True)
#
#     def __str__(self):
#         return '{} {}'.format(self.amount, self.balance)
#
#
# class UserBalance(models.Model):
#     user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
#     balance = models.IntegerField(default=0)
#
#     def __str__(self):
#         return '{} {}'.format(self.user, self.balance)


class Call(models.Model):
    NEW = 'NEW'
    DONE = 'DONE'
    CANCELED = 'CANCELED'
    STATUS_CHOICES = (
        (NEW, 'New'),
        (DONE, 'Done'),
        (CANCELED, 'Canceled'),
    )

    name = models.CharField(max_length=124)
    phone_number = PhoneNumberField()
    description = models.CharField(max_length=300, blank=True)
    status = models.CharField(max_length=12, choices=STATUS_CHOICES, default='NEW')

    def __str__(self):
        return '{} {}'.format(self.phone_number, self.name)
