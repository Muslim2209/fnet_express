# Generated by Django 3.0.1 on 2019-12-26 18:45

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('areas', '0001_initial'),
        ('delivery', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='courier',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='courier', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='order',
            name='driver',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='driver', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='order',
            name='from_to',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='delivery.FromTo'),
        ),
        migrations.AddField(
            model_name='order',
            name='tariff',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='delivery.Tariff'),
        ),
        migrations.AddField(
            model_name='fromto',
            name='from_point',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='fromCity', to='areas.City'),
        ),
        migrations.AddField(
            model_name='fromto',
            name='to_point',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='toCity', to='areas.City'),
        ),
        migrations.AlterUniqueTogether(
            name='fromto',
            unique_together={('from_point', 'to_point')},
        ),
    ]
